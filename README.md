# ModJam5_ChaosLizzies
A mod project for Total War Warhammer 3.


## A mod project for DaModdingDen's Mod Jam 5 - Kill Your Darlings

This project is a collaboration between HvnterVMLV, Nanu, UlrikHD and "the thewormjb".

Texture work: Nanu and HvnterVMLV, with some assistance from the thewormjb

Databases: UlrikHD, HvnterVMLV and Nanu

Scripts: UlrikHD

The published packfile can be found on the Total War Warhammer 3 steam workshop page: [Mod Jam 5 Presents: Chaos Lizards](https://gitlab.com/UlrikHD/modjam5_chaoslizzies/-/settings/integrations)

## Mod description
This is a mod made for Da Modding Den's Mod Jam 2022 5Y Anniversary event: Kill your Darlings.

"Your task with this Mod Jam is to take 10 days to desecrate something you love. 
Whether it's a character you like, an interesting piece of lore, a functioning mechanic - kill it. 
Make it unrecognizable, twisted, bad, or just wrong."

So, we did something something heretical and made Chaos Lizardmen. 
This is a faction overhaul that converts Kroq-Gar's faction to a chaos aligned race with new Chaos Lizardmen units and flavor, as well as some visual overhauling.


## Authors and acknowledgment
Ole for developing [Asset Editor](https://github.com/donkeyProgramming/TheAssetEditor) which is an essential tool for kit bashing units.

Frodo45127 for developing [Rusted Pack File Manager](https://github.com/Frodo45127/rpfm), a vital program for any sort of Total War modding.

Groove Wizard for supplying the [tw_autogen](https://github.com/chadvandy/tw_autogen) for easier development of Lua scripts integrated into the Total War Warhammer 3 script framework.

[Creative Assembly](https://www.creative-assembly.com/) for developing Total War Warhammer 3 and for truly supporting the modding community.

## License
Distributed under the MIT License. See [LICENSE](https://gitlab.com/UlrikHD/modjam5_chaoslizzies/-/blob/main/LICENSE) for more information.

## Project status
The mod was made for Mod Jam 5 - Kill Your Darlings and will not be developed much further after the event is over.
