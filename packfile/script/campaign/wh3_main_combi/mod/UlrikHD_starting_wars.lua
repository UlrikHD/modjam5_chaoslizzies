local function start_wars()
    local faction = "wh2_main_lzd_last_defenders"
    cm:force_declare_war(faction, "wh3_main_ksl_the_ice_court", false, false)
    cm:force_declare_war(faction, "wh3_main_ksl_ropsmenn_clan", false, false)
    cm:force_make_peace(faction, "wh2_main_skv_clan_mordkin")
    --- Remove peace treaty with wh2_main_hef_order_of_loremasters
    cm:force_declare_war(faction, "wh2_main_hef_order_of_loremasters", false, false)
    cm:force_make_peace(faction, "wh2_main_hef_order_of_loremasters")
end



cm:add_first_tick_callback(
    function()
        if cm:is_new_game() then
            start_wars()
        end
    end
)
