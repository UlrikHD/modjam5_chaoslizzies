local starting_army_new = {
    "chs_saurus_warriors",
    "chs_saurus_warriors",
    "tze_saurus_warriors_halberds",
    "kho_skink_cohort_great_weapons",
    "kho_skink_cohort_great_weapons",
    "sla_cold_one_spear_riders",
    "hv_lzd_mon_bastiladon_3",
}

local starting_army_old = {
    "wh2_main_lzd_inf_saurus_spearmen_0",
    "wh2_main_lzd_inf_saurus_spearmen_0",
    "wh2_main_lzd_inf_saurus_warriors_blessed_1",
    "wh2_main_lzd_inf_skink_cohort_1",
    "wh2_main_lzd_inf_skink_cohort_1",
    "wh2_main_lzd_cav_cold_one_spearmen_1",
    "wh2_main_lzd_mon_stegadon_0",
}

local enemy_starting_army = {
    "wh3_main_ksl_inf_armoured_kossars_1",
    "wh3_main_ksl_inf_armoured_kossars_1",
    "wh3_main_ksl_inf_armoured_kossars_1",
    "wh3_main_ksl_inf_tzar_guard_1",
    "wh3_main_pro_ksl_inf_kossars_0",
    "wh3_main_ksl_cav_horse_archers_0",
}

local faction = "wh2_main_lzd_last_defenders"
local region = "wh3_main_combi_region_karak_vlag"

local function spawn_agents()
    local x, y = cm:find_valid_spawn_location_for_character_from_settlement(faction, region, false, true, 4)
    cm:create_agent(faction, "wizard", "chs_skink_priest_tzeentch", x, y, "", false)
    x, y = cm:find_valid_spawn_location_for_character_from_settlement(faction, region, false, true, 5)
    cm:create_agent(faction, "champion", "sla_lzd_saurus_scar_veteran", x, y, "", false)
    
end


local function kill_starting_hero()
    local faction_interface = cm:get_faction(faction)
    local char_list = faction_interface:character_list()
    for _, character in model_pairs(char_list) do
        if character:character_type_key() == "wizard" then
            cm:kill_character(character:cqi(), false)
        end
    end
end


---@return string
local function list_to_string(list)
    local str = list[1]
    for i = 2, #list do
        str = str .. "," .. tostring(list[i])
    end
    return str
end


local function enemy_army_spawn()
    local faction_name = "wh3_main_ksl_ropsmenn_clan"
    local x, y = cm:find_valid_spawn_location_for_character_from_settlement(faction_name, region, false, true, 10)
    local general__type = "general"
    local general__subtype = "wh3_main_ksl_boyar"
    local general_forename = ""
    local general_clanname = ""
    local general_surname = ""
    local general_othername = ""
    local general_is_faction_leader = false
    cm:create_force_with_general(
        faction_name,
        list_to_string(enemy_starting_army),
        region,
        x,
        y,
        general__type,
        general__subtype,
        general_forename,
        general_clanname,
        general_surname,
        general_othername,
        general_is_faction_leader,
        function(cqi)
            return true
        end
    )
end


local function change_starting_army()
    custom_starts:modify_units_in_army(
        faction,
        300,
        300,
        starting_army_new,
        starting_army_old,
        nil,
        nil
    )
end


cm:add_first_tick_callback(
    function()
        if cm:is_new_game() then
            core:get_tm():real_callback(
                function()
                    enemy_army_spawn()
                    kill_starting_hero()
                    spawn_agents()
                    change_starting_army()
                end,
            20
        )
        end
    end
)
