local function unlock_skink_technology()
    local technology_key = "wh2_main_tech_lzd_6_1"
    local faction = "wh2_main_lzd_last_defenders"
    cm:instantly_research_technology(faction, technology_key, false)
end


cm:add_first_tick_callback(
    function()
        if cm:is_new_game() then
            unlock_skink_technology()
        end
    end
)
