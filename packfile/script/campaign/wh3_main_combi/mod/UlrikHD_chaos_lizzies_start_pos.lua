local function teleport_characters(faction_key, region_key)
    --- Transfer the settlement
    math.randomseed(os.clock())
    local new_owner_model = cm:get_faction(faction_key)
    local new_owner_model_character_list = new_owner_model:character_list()
    ---Teleport characters
    for i = 0, new_owner_model_character_list:num_items() - 1 do
        local character = new_owner_model_character_list:item_at(i)
        local distance = math.random(1, 6)
        local x, y =
            cm:find_valid_spawn_location_for_character_from_settlement(
            faction_key,
            region_key,
            false,
            true,
            distance
        )
        if x > 0 then
            cm:teleport_to("character_cqi:" .. tostring(character:cqi()), x, y, true)
        end
    end
end

local function update_settlement()
    local faction_key = "wh2_main_lzd_last_defenders"
    local region_key = "wh3_main_combi_region_karak_vlag"
    local temp_owner = "wh2_main_skv_clan_mordkin"
    local settlement_level = 3
    core:get_tm():real_callback(
        function()
            cm:transfer_region_to_faction(region_key, temp_owner)
        end,
        5
    )
    core:get_tm():real_callback(
        function()
            cm:transfer_region_to_faction(region_key, faction_key)
            cm:instantly_set_settlement_primary_slot_level(cm:get_region(region_key):settlement(), settlement_level)
            cm:heal_garrison(cm:get_region(region_key):cqi())

            if region_key ~= "wh3_main_combi_region_the_golden_tower" then
                teleport_characters(faction_key, region_key)
                cm:set_region_abandoned("wh3_main_combi_region_the_golden_tower")
            end
        end,
        10
    )
end

cm:add_first_tick_callback(
    function()
        if cm:is_new_game() then
            update_settlement()
        end
    end
)
